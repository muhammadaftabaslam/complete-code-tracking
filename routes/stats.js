/*
 * Stats Saving and Serving over socket. 
 * Author: M. Aftab Aslam
 * copyrigths: liveadmins.com LLC
 */
var events = require('events'),
util = require('util'), url = require('url');
var crypto = require('crypto');
var async = require('async'); 
colors = require('colors');
var _ = require('underscore');
var fs = require('fs');
var path = require('path');

var track = require('../lib/tracking');
redisio = require('../lib/redis');
	sk_stats = require('../lib/sk_stats'),
	bt_stats = require('../lib/bt_stats');
	tm_stats = require('../lib/tm_stats');

var ValidatePostDataJson = function(data,callback){
	try
	{
		var parsedData = JSON.parse(data);	
		var d = new Date();
		parsedData['datetime'] = d.toUTCString();		
		callback(parsedData);
	} 
	catch (e) 
	{
		console.error("Stats : ValidatePostDataJson : Parsing error:", e); 	
				
	}
}

 var switchPostType = function(req,res){
 try{
	
	res.end("ok",function(){
			ValidatePostDataJson(req.body.data,function(parsedData)
			{
			if(parsedData.visitorid != undefined)
			{
			switch(parsedData.postType)
			{
				case 'BT':
							bt_stats.BehavoirTracking(parsedData); 										
							break;	
				case 'SK':						
							sk_stats.SearchKeywordsTracking(parsedData); 																				
							break;
				case 'TM':	
					saveTrackingInfo(req,res,parsedData);
					tm_stats.TimeTracking(parsedData); 	
							break;
				case 'Chat':	
							track.saveChatTime(parsedData); 										
							break;
				default:
				 console.log("Unknown Type of data or not parsed in the proper way".red);
				}
			}
			});
		});
	}
	catch(er)
	{
		console.log("Stats : switchPostType : ",er);
	}
}

var saveTrackingInfo = function(req,res,parsedData)
	{
		try{
			track.savingTrackingInfo(req,res,parsedData,function(){
			//console.log("IP info saved");
			});
		}	
		catch(er)
		{
			console.log("Stats : saveTrackingInfo : ",er);
		}
	}	    

exports.ajaxPost = function(req,res){
	try
	{
		var queryString = url.parse(req.url,true).query;
		if(_.isEmpty(queryString)){
			switchPostType(req,res);
		}
		else{
			//console.log('GET Request with query string  for IE = ',queryString);
			req.body.data = queryString.data;
			console.log("IE Request found***************".green );
			switchPostType(req,res);
		} 
	}	
		catch(er)
		{
			console.log("Stats : ajaxpost : ",er,data);
		}
}

