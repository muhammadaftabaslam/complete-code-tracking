var redisio = require('./redis');
var childProcess = require("child_process");
var util = require('util');
//var _ = require('underscore');

exports.hashExists = function(visitorid,callback){ // check Hash key exist or not
 redisio.CheckKey("Track:"+visitorid,function(status){
 callback(status);
 });
}
exports.ActivityList = function(data,callback){ // check Hash key exist or not
try{
	//data.actionurl = _.escape(data.actionurl);
	
  redisio.PushDataToList("Act:"+data.postType,data,function(){
  callback();
  });
 }
 
 catch(er)
 {
  console.log("Stats_lib : ActivityList : ",er);
 }
}


function SessionClose (visitorid,callback){ // Session close function
 redisio.SessionClose(visitorid,function(){
 callback();
 });
}


exports.checkifSessionClosedTMIsReceived = function(session,callback){ // Check if session closed TM is received
 try{
  if(session === true)
  {
   callback(true);
  }
  else
  {
   callback(false);
  } 
 }
 catch(er)
 {
  console.log("Stats_lib : checkifSessionClosedTMIsReceived : ",er);
 }
}



exports.DataPushToOperator = function(visitorid,callback){ // function for pushing data to operator
 redisio.DataPushToOperator(visitorid,function(data){
 callback(data);
 });
}
exports.runningCron = function(){
 try
 {
		console.log("child process start".red);
		
		this._retrieveChild = childProcess.fork("./lib/src/cron");
		var _finalizedData = null,
		_httpRequestArray = ["http://someurl","http://someurl2","http://someurl3"];
	 
	var data = {
		"start":true,
		"interval": 8 * 60 * 60 * 1000,
		"intervalforlen":  5 * 60 * 1000,
		"content": _httpRequestArray
	}
	 console.log("Sending data from stats.js");
	this._retrieveChild.send(data);

	
	this._retrieveChild.on('message', function(msg){
    console.log("Recv'd message from background process.");
    _finalizedData = msg.content;
	console.log("final data".blue,_finalizedData);
}.bind(this));

 }
 catch(er)
 {
  console.log("Stats_lib : runningCron : ",er);
 }
}