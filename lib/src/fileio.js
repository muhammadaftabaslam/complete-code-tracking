fs = require('fs');

function getNowDate(){ 
	try
	{
		//console.log("FileIO : func getNowDate Start");
		var date = new Date;
		var month = date.getUTCMonth() + 1;
		var day = date.getUTCDate();
		var year = date.getUTCFullYear();
		var nowDate = day+"_"+month+"_"+year;
		//console.log("date at this time",nowDate);
		//console.log("FileIO : func getNowDate end");
		return nowDate;
	}
	catch(err)
	{
		console.log("FIleIO : GetNowDate : ",err);
	}
}
exports.InsertInFile = function (data,fileName,callback){
	console.log("FileIO : func InsertInFile Start");
	//console.log("writing file",fileName,data);
	try{
		var date = new Date;
		fs.exists("./CSV/"+getNowDate()+"_"+fileName+".csv", function (exists) {
			if(exists)
			{
				console.log("FileIO Already exist");
				fs.writeFile("./CSV/"+getNowDate()+"_"+fileName+"_"+date.getTime()+".csv",data, function(err) {
				if(err) {
					console.log("Error FileIO FILE NOT SAVED : ",err);
				} else {
					console.log("FileIO The file was saved!");
					callback();
				}
			});
			}
			else
			{
				console.log(" FileIO not Already exist");
				fs.writeFile("./CSV/"+getNowDate()+"_"+fileName+".csv",data, function(err) {
				if(err) {
					console.log("Error FileIO FILE NOT SAVED : ",err);
				} else {
					console.log("FileIO The file was saved!");
					callback();
				}
			});
			}
		});
	}
	catch(er)
	{
		console.log("FIleIO : InsertInFile : ",er);
	}
}
exports.appendFile = function(data,fileName){
	try
	{
		fs.appendFileSync("./CSV/"+getNowDate()+"_"+fileName+".csv",data);
	}
	catch(er)
	{
		console.log("FIleIO : appendFile : ",er);
	}

}

exports.InsertInFileforBasicInfo = function (data,callback){
	try{
		//console.log("FileIO for INFO");
				
		fs.appendFileSync("./CSV/Info_"+getNowDate()+".csv",data);

	}
	catch(er)
	{
		console.log("FIleIO : InsertInFile : ",er);
	}
}