var csv = require('./csv_code');
var stats_lib = require('./stats_lib'),
	fileio = require('./fileio');
	socketio = require('./socketServer');
	
exports.SearchKeywordsTracking = function(data){
	try
	{
	//console.log('Searched Keywords Data '.green,data);
	redis.hset("Track:"+data.visitorid,"SearchTracking",'SK:'+data.visitorid);// set field in hash 
	redis.lpush('SK:'+data.visitorid,JSON.stringify(data)); // Push data in the SK list
	var visitordata= {};
	var activities = [];
	activities.push(data);
		visitordata.SK = activities;
		visitordata.Id = data.visitorid;
		visitordata.VT = [];
		visitordata.VLF = [];
		visitordata.BT = []; 
		//stats_lib.isOperatorListening(data.visitorid,visitordata,function(){});
		socketio.CurrentData(data.visitorid,visitordata,function(){});
		csv.InsertInString(data,function(){});
	}
	catch(er)
	{
		console.log("SK_stats : SearchKeywordsTracking : ",er);
	}
	
}