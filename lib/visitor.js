var redisio = require('./redis');
var stats_lib = require('./stats_lib');
var fileio = require('./fileio');
var csv = require('./csv_code');
function FindDataFromTmStats(data){
	try
	{
		var obj = {};
		if(data["productURL"] == true)
		{
			obj.VisitorId = data.visitorid;
			//console.log("time is less than 5".red);
			if(data["type"] != "" && data["type"] != undefined)
			{
				//console.log("type".red);
				obj["type"] = data["type"]
			}
			if(data["year"] != "" && data["year"] != undefined)
			{
				//console.log("year".red);
				obj["year"] = data["year"]
			}
			if(data["make"] != "" && data["make"] != undefined)
			{
				//console.log("make".red);
				obj["make"] = data["make"]
			}
			if(data["model"] != "" && data["model"] != undefined)
			{
				//console.log("model".red);
				obj["model"] = data["model"]
			}
			return obj;
		}
		else
		{
			//console.log("have no data for ".red);
		}
		
	} 
	catch (e) 
	{
		console.log("Visitor.js Exception ".red,e);
	}
}


exports.SaveVisitorTMStats = function(data,callback)
{
	try
	{
		//console.log("Visitor.js : SaveVisitorTMStats start");
		var LandingTime = new Date();
		LandingTime.setSeconds(LandingTime.getSeconds() - parseInt(data.time));
		
		if (data["active"] === true)// IF PAGE IS ACTIVE
		{
			//console.log("Visitor.js : Page Active TM"); // IF PAGE IS ACTIVE
			var obj = {};
			obj = FindDataFromTmStats(data);
			var activities = [];
			var visitordata= {};
			var data1 = {};
			data1.URL = data.actionurl;
			data1.TimeSpend = data.time.toString();
			data1.VisitorId = data.visitorid.toString();
			data1.Status = "1";
			data1.datetime = LandingTime;
			activities.push(data1);
			visitordata.VT = activities;
			visitordata.Id = data.visitorid;
			visitordata.SK = []; 
			visitordata.VLF = [];
			visitordata.BT = [];
					//console.log('if ( obj != undefined = ', ( obj != undefined));
			if(obj != undefined)
			{
				visitordata.VLF.push(obj);
			}
			socketio.CurrentData(data.visitorid,visitordata,function(){});
			redis.zscore('TM-Current:'+data.visitorid,data.actionurl,function(err,reply){
						if(!err){
			redis.expire('TM-Current:'+data.visitorid,150);
			//console.log("zcrore".yellow,reply);
				if(reply > data.time)// CHECK SCORE OF THE SAME URL IT IT EXIST
				{
					// Do nothing, No need to update data
					callback();
				}
				else
				{
					redis.zadd('TM-Current:'+data.visitorid, data.time, data.actionurl,function(){// PAGE TIME UPDATE
						callback();
					});
				}
						}else{
						   // console.log('ZSCORE ERROR.'.red);
						}
			}); 
		}
		else
		{
			//console.log("Visitor.js : Page Close TM"); // IF PAGE IS INACTIVE
			var activities = [];
			var visitordata= {};
			var data1 = {};
			data1.URL = data.actionurl;
			data1.TimeSpend = data.time.toString();
			data1.VisitorId = data.visitorid.toString();
			data1.datetime = LandingTime;
			data1.Status = "0";
			activities.push(data1);
			visitordata.VT = activities;
			visitordata.Id = data.visitorid;
			visitordata.SK = []; 
			visitordata.BT = []; 
			visitordata.VLF = [];
			data.datetime = LandingTime;
			socketio.CurrentData(data.visitorid,visitordata,function(){});
			
			
			redis.zrem('TM-Current:'+data.visitorid,data.actionurl,function(err,val){
			if(!err)
			{
				redis.zincrby("TM-History:"+data.visitorid,data.time,data.actionurl);
			}
				
				else{
					console.log("Visitor.js zrem: zincrby",err,val);
				}
			});
			csv.InsertInString(data,function(){});
			

			
			
		}
	}
	catch(er)
	{
		console.log("visitor : SaveVisitorTMStats : ",er);
	}
}













