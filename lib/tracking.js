var redisio = require('./redis');
var url = require('url');
var _ = require('underscore');


exports.savingTrackingInfo = function (req,res,data,callback){

var UAParser = require('ua-parser-js');
var ua = req.headers['user-agent'];
var parser = new UAParser();

var browserInfo = "";//parser.setUA(ua).getBrowser().name +" " +parser.setUA(ua).getBrowser().version;//+" " + parser.setUA(ua).getBrowser().major;	
var osInfo ="";//parser.setUA(ua).getDevice().vendor+" " +	parser.setUA(ua).getDevice().model+" " +	parser.setUA(ua).getDevice().type;
var Engine ="";//parser.setUA(ua).getEngine().name+" " +	parser.setUA(ua).getEngine().version;
var vstrOSys ="";//parser.setUA(ua).getOS().name+" " +	parser.setUA(ua).getOS().version;
  
	/*BrowserInfo*/
	var BrowserName=((typeof parser.setUA(ua).getBrowser().name !=="undefined") ? parser.setUA(ua).getBrowser().name : "");
	var BrowserVersion=((typeof parser.setUA(ua).getBrowser().version !=="undefined") ? parser.setUA(ua).getBrowser().version : "");
	browserInfo=BrowserName +" " + BrowserVersion;
	
	
	/*Device/OS Info*/
	var DeviceVendor=((typeof parser.setUA(ua).getDevice().vendor !=="undefined") ? parser.setUA(ua).getDevice().vendor : "");	
	var DeviceModel=((typeof parser.setUA(ua).getDevice().model !=="undefined") ? parser.setUA(ua).getDevice().model : "");
	var DeviceType=((typeof parser.setUA(ua).getDevice().type !=="undefined") ? parser.setUA(ua).getDevice().type : "");
	osInfo=DeviceVendor +" " + DeviceModel+" " +DeviceType;
	
	/*Engine Info*/
	var EngineName=((typeof parser.setUA(ua).getEngine().name !=="undefined") ? parser.setUA(ua).getEngine().name : "");
	var EngineVersion=((typeof parser.setUA(ua).getEngine().version !=="undefined") ? parser.setUA(ua).getEngine().version : "");
	Engine=EngineName +" " + EngineVersion;
	
	/*Operating System Info*/
	var OsName=((typeof parser.setUA(ua).getOS().name !=="undefined") ? parser.setUA(ua).getOS().name : "");
	var OsVersion=((typeof parser.setUA(ua).getOS().version !=="undefined") ? parser.setUA(ua).getOS().version : "");
	vstrOSys=OsName +" " + OsVersion;
	
/*Check is it Mobile Device*/
var isMobile=false;
var Android, Mobile, iPhone;
    try{
     
      iPhone = ua.match(/iPhone/i) || ua.match(/iPod/i);
      Android = ua.match(/Android/i);
      Mobile = ua.match(/Mobile/i);
      isMobile = (iPhone || (Android && Mobile));      
    }catch(err){
      isMobile = false;      
    }
	
var user = {
    vstrAgent: ua, // User Agent we get from headers
    vstrReferrer: ((typeof req.headers['referer'] !=="undefined") ? req.headers['referer'] : ""), //  Likewise for referrer
    vstrIp: (req.headers['x-forwarded-for'] || '').split(',')[0]  || req.connection.remoteAddress,//IP
	vstrOS:vstrOSys, //Operating System Name and Version
	vstrIsMobile:isMobile,
	vstrBrowser:browserInfo,//BrowserName +" " + BrowserVersion;
	VstrMachineInfo:osInfo //DeviceVendor +" " + DeviceModel+" " +DeviceType;
  };
	
	var multi = redis.multi();
	redis.hsetnx("visitorIdsForHashes",data.visitorid,1);
	multi.hsetnx("Track:"+data.visitorid,"VisitorID",data.visitorid);
	multi.hsetnx("Track:"+data.visitorid,"IP",user.vstrIp);
	multi.hsetnx("Track:"+data.visitorid,"OS",user.vstrOS);
	multi.hset("Track:"+data.visitorid,"Browser",user.vstrBrowser);
	multi.hsetnx("Track:"+data.visitorid,"MInfo",user.VstrMachineInfo);
	multi.hsetnx("Track:"+data.visitorid,"IsMobile",user.vstrIsMobile);
	multi.hsetnx("Track:"+data.visitorid,"Referrer", user.vstrReferrer);
	multi.hsetnx("Track:"+data.visitorid,"URL",data.websiteurl);
	multi.hsetnx("Track:"+data.visitorid,"BrowserEngine",Engine);
	multi.hsetnx("Track:"+data.visitorid,"WebsiteId",data.websiteid);
	multi.hsetnx("Track:"+data.visitorid,"DateTime",data.datetime);
	multi.hsetnx("Track:"+data.visitorid,"count",0);
	multi.hsetnx("Track:"+data.visitorid,"startTime","");
	multi.hsetnx("Track:"+data.visitorid,"endTime","nothing");
	multi.hsetnx("Track:"+data.visitorid,"chatDuration",0);

	multi.exec(function(error, results)
    {
		if(error)
		{
			console.log("Tracking.js Error  :", error); 
		}
		else
		{
			callback();
		}
	});
	
}

exports.checkBrowser = function(req,res){
	res.end("ok");
	console.log("inside check browser");
	var queryString = url.parse(req.url,true).query;
	//console.log("DATA",queryString.data);
	if(_.isEmpty(queryString))
	{
		//console.log("CHAT DATA==========================".red,req.body.data);
		//console.log("NOT FROM IE");
        saveChatTime(req,res);
    }
    else
	{
        //console.log('GET Request with query string  for IE = ',queryString);
		//console.log("CHAT DATA==========================".red,queryString.data);
		req.body.data = queryString.data;
		saveChatTime(req,res);
    } 
}

exports.saveChatTime = function(data){
	//var data = JSON.parse(req.body.data);
	//console.log("chat data",data);
	if(data.startTime != "")
	{
		var startTime = new Date(data.startTime);
		//console.log("saving start time");
		redis.hincrby("Track:"+data.visitorid,"count",1,function(err)
		{
			if(err)
			{
					console.log("err : Tracking.js :  Get Hash Field".red, err);
			}
		});
		redis.hset("Track:"+data.visitorid,"startTime",startTime,function(err)
		{
			if(err)
			{
					console.log("err : Tracking.js :  Get Hash Field".red, err);
			}
		});
		redis.hset("Track:"+data.visitorid,"endTime","nothing",function(err)
		{
			if(err)
			{
					console.log("err : Tracking.js :  Get Hash Field".red, err);
			}
		});
	}
	if(data.endTime != "")
	{
		var endTime = new Date(data.endTime);
		//console.log("saving end time");
		redis.hset("Track:"+data.visitorid,"endTime",endTime,function(err){
			if(err)
			{
				console.log("err : Tracking.js :  Get Hash Field".red, err);
			}
		});
		redis.hget("Track:"+data.visitorid,"startTime",function(err,value){
			if(err)
			{
				console.log("err : Tracking.js :  Get Hash Field".red, err);
			}
			else
			{
				//console.log("start time get from redis",value);
				//console.log("end time get from user call",data.endTime);
				var startTime = new Date(value);
				var endTime = new Date(data.endTime);
				//console.log("Start time", startTime.getTime());
				//console.log("End time", endTime.getTime());
				//console.log("Differnence actula", endTime.getTime() - startTime.getTime() );
				var diff = (endTime.getTime() - startTime.getTime())/1000;
				console.log("Differnence", diff);
				diff = diff.toFixed(2);
				diff = parseInt(diff);
				console.log("differnece",diff);
				if(diff<1){diff=1;}
				
				redis.hincrby("Track:"+data.visitorid,"chatDuration",diff,function(err)
				{
					if(err)
					{
						console.log("err : Tracking.js :  Get Hash Field".red, err);
					}
				});
				
			}
		
		});
	}
	
}

exports.calculateEndTime = function(visitorid,count,callback){
		console.log("inside calculateEndTime",visitorid);
		redisio.getString(visitorid+":LastPostTime",function(result){
			console.log("last time",result);
			//redis.hset("Track:"+visitorid,"endTime",result,function(err){console.log("err",err);});
			redis.hget("Track:"+visitorid,"startTime",function(err,value){
				if(err)
				{
					console.log("err : Tracking.js :  Get Hash Field".red, err);
					callback(visitorid,count);
				}
				else
				{
					var startTime = new Date(value);
					var endTime = new Date(result);
					console.log("startTime",startTime);
					console.log("endTime",endTime);
					var diff = (endTime.getTime() - startTime.getTime())/1000;
					//diff = diff.toFixed(2);
					diff = parseInt(diff);
					if(diff<1)diff=1;
					console.log("INCRBY",diff);
					redis.hincrby("Track:"+visitorid,"chatDuration",diff,function(err){
					if(err)
					{
						console.log("err : Tracking.js :  Get Hash Field".red, err);
					}
					});
					callback(visitorid,count);
				}
			
			});
		});
}